# A free and open source learning and content management software
Claims is an FOSS licensed learning and content management software that can be federated with not just other Claims instances. It was created because of frustration from proprietary cloud-based services, that are very buggy and often hard to use.

## Features
- Free and open source, forever
- Can be [federated](https://fedi.info), not just with other instances
- Very fast, no JavaScript involved, only HTML and CSS
- Written in Rust.
- Very customizable
- Extensible

## Installation
Run `cargo install --locked claims` for the stable version or `cargo install --locked --git https://git.guymatus.tech/TenTypekMatus/Claims` for the latest and greatest version.

## Development
Since this is pure Rust (except the HTML code), the development's easy. It's just matter of cloning the code and working on the code. Everything else is handled by Cargo.
