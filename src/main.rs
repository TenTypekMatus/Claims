mod structs;
mod enums;
use rocket::get;
extern crate rocket_contrib;
use rocket_dyn_templates::context;
use rocket_dyn_templates::Template;
#[macro_use]
extern crate rocket;
#[get("/")]
async fn index() -> Template {
    Template::render(
        "index",
        context! {
            title: "Front of a Claims instance"
        },
    )
}
#[catch(404)]
fn notfound() -> &'static str {
    r#"There's nothing to see here"#
}
#[catch(500)]
fn cfgerr() -> &'static str {
    r#"Wrong configuration, fix it."#
}
#[get("/timetable")]
async fn timeline() -> Template {
    Template::render(
        "timetable",
        context! {
            title: "Timetable",
        },
    )
}
#[get("/messages")]
async fn messages() -> Template {
    Template::render(
        "messages", context! {
            title: "Messages",
            desc: "View your messages, homeworks and more on one timeline"
        },
        )
}
#[get("/profile/<username_handle>")]
async fn profile(username_handle: &str) -> Template {
    let header = format!("{username_handle}@Claims");
    Template::render(
        "profile", context! {
            title: header,
            desc: "Change your profile picture, description, your handle and your profile background"
        },
        )
}
#[launch]
fn rocket() -> _ {
    rocket::build()
        .mount("/", routes![index, timeline, messages, profile])
        .attach(Template::fairing())
        .register("/", catchers![notfound, cfgerr])
}
