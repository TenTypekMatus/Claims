use handlebars::Handlebars;
enum TypesStudentParent {
    Homework,
    Excuse,
    DMLocal,
    /// Messages are sent through the Matrix protocol
    DMFederated,
}

enum TypesTeacherStaff<'a> {
    SmallTest(&'a str),
    MediumTest(&'a str),
    BigExam(&'a str),
    Notice(&'a str),
    Compliment(&'a str),
    AnnouncementLocal,
    /// Messages are sent through ActivityPub.
    AnnouncementFederated(&'a str),
    Homework(&'a str),
    Excuse(&'a str),
    MessageLocal(&'a str),
    MessageFederated(&'a str),
}
impl TypesTeacherStaff<'_> {
    fn small_test(msg: &mut Handlebars) {
        todo!();
    }
}
