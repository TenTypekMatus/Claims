struct Roles {
    can_access_db: bool,
    first_name: String,
    last_name: String,
    instance_name: String,
    /// Level of permissions:
    /// - 1 is the lowest level, intended for students
    /// - 2 is better, intended for students over 18
    /// - 3 is for parents
    /// - 4 is for teachers
    /// - 5 is for school staff
    /// - 6 is for administrators
    permission_level: u8,
}
